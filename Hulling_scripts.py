from lxml import etree
import re
import string

punct = r"[.,?!:;'-]"
rx = r"""[^а-я\s]"""
xml = []

def read_file(filename):
	with open(filename, encoding='utf-8') as corpus:
		return corpus.read().lower()

def write_result(text):
	with open("out.txt", 'w', encoding='utf-8') as out:
		out.write(text)

def get_half(text):
	return text.split('\n')[::2]

def test_word(word):
	for c in word:
		if (re.match(r"[0-9а-яА-Я]", c)):
			continue
		if (c in string.whitespace):
			continue
		if (c in string.punctuation):
			continue
		return False
	return True

def hull_learning_sample_strict(text):
	res = []
	strict_punct = re.compile('^[а-я .,?!:"-]+$')
	for sentence in text.split('\n'):
		if (strict_punct.match(sentence)):
			res.append(sentence)
			
	return res

def observe_datasets():
	names = ["war_and_peace.txt", "silver_standard.txt", "corpus.ru.txt"]
	for filename in names:
		print("\n" + filename)
		text = read_file(filename)
		print(text[:50])
		print("Число слов: " + str(len(text.split(' '))))
		print("Число различных символов: " + str(len(list(set(text)))))

def hull_war_and_peace():
	filename = "war_and_peace.txt"
	text = read_file(filename)
	rx = r"""[^а-я\s]"""
	text = re.sub(rx, "", text)
	text = re.sub(r"""[\s]{2,}""", " ", text)
	write_result(text)

def hull_silver_standard():
	filename = "test_sample.strict.txt"
	text = read_file(filename)
	text = re.sub(rx, "", text)
	write_result(text)

def hull_yandex_translate():
	filename = "corpus.ru.strict.half.txt"
	text = read_file(filename)
	text = re.sub(rx, "", text)
	text = text.replace("\n", "")
	write_result(text)

def prepare_test_sample(size=0):
	filename = "corpus.ru.strict.txt"
	text = []
	with open(filename, encoding='utf-8') as corpus:
		text = corpus.readlines()
	text = text[1::8]
	final_sentences = []
	for sentence in text:
		sentence = re.sub(rx, "", sentence)
		if (len(sentence.split(" ")) > 4):
			final_sentences.append(sentence)
	if (size == 0):
		size = len(final_sentences)
	write_result("".join(final_sentences[:size]))

def main():
	observe_datasets()
	hull_war_and_peace()
	hull_silver_standard()
	hull_yandex_translate()
	prepare_test_sample()


'''
Entrance point into script
Please, define all your logic as functions
and call them from main function body
'''
main()