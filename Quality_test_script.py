# Загружаем обученную сеть
model = create_model(context_length, len(chars))

# одно из трёх
model.load_weights('/content/gdrive/My Drive/saved_nnets/war_and_peace/war-and-peace.txt-14.hdf5')
model.load_weights('/content/gdrive/My Drive/saved_nnets/corpus_ru/corpus.ru.txt-19.hdf5')
model.load_weights('/content/gdrive/My Drive/saved_nnets/silver_standard/silver-standard-21.hdf5')

# Загружаем тестовую выборку
test_path = "/content/gdrive/My Drive/saved_nnets/test_sample.txt"

# Читаем тестовую выборку в память
test_sample = []
with io.open(test_path, encoding='utf-8') as f:
    test_sample = f.readlines()

# "Отрезаем" окончания у слов
predicted_sample = []
for sentence in test_sample:
    words = sentence[:-1].split(" ")
    if (len(words) < sentence_length):
        continue;
    clipped = words[initial_words:]
    
    for i, word in enumerate(clipped):
        if (len(word) > ending_length + 3):
            clipped[i] = word[:-ending_length]
    
    final_sentence = words[:initial_words] + clipped
    predicted_sample.append(" ".join(final_sentence))

# Определяем функцию предсказания окончаний
def predict_ending(base, word):
    for i in range(ending_length):
        x_pred = np.zeros((1, context_length, len(chars)))
        for t, char in enumerate(base):
            if char in char_indices.keys():
                x_pred[0, t, char_indices[char]] = 1.

        prediction = model.predict(x_pred, verbose=0)[0]
        next_index = np.argmax(prediction)
        next_char = indices_char[next_index]

        word += next_char
        base = base[1:] + next_char
        
    return word

# Запускаем предсказание на тестовой выборке
total_predicted_words = 0
right_predicted_words = 0

for (t, p) in zip(test_sample, predicted_sample):
    words = p.split(" ")
    test_words = t.split(" ")
    
    for i in range(len(words) - initial_words):
        base = words[:(initial_words + i)]
        word = words[initial_words + i]
        if (len(word) > 2 + ending_length):
            base = " ".join(base) + " " + word
            base = base[-context_length:]
            word = predict_ending(base, word)
            
            total_predicted_words += 1
            test_word = t.split(" ")[initial_words + i]
            if (word == test_word):
                right_predicted_words += 1
                
# Точность системы, %
print(right_predicted_words / total_predicted_words)

# Строим графики метрики Loss function
training_loss = history.history['loss']
test_loss = history.history['val_loss']
epoch_count = range(1, len(training_loss) + 1)

plt.plot(epoch_count, training_loss, 'r--')
plt.plot(epoch_count, test_loss, 'b-')
plt.legend(['Training Loss', 'Test Loss'])
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.grid()
plt.show();

# Строим графики метрики Accuracy
training_acc = history.history['accuracy']
test_acc = history.history['val_accuracy']
epoch_count = range(1, len(training_acc) + 1)

plt.plot(epoch_count, training_acc, 'r--')
plt.plot(epoch_count, test_acc, 'b-')
plt.legend(['Training Acc', 'Test Acc'])
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.grid()
plt.show();