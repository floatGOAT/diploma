# Устанавливаем tensorflow-gpu версии 2.0.0 чтобы пользоваться всеми возможностями параллельных вычислений на GPU
pip install tensorflow-gpu==2.0.0-alpha0
pip install keras==2.2.4

# Подключаем все необходимые зависимости
from tensorflow.keras import backend as K
from tensorflow.keras.callbacks import LambdaCallback, ModelCheckpoint, EarlyStopping
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, Reshape
from tensorflow.keras.optimizers import RMSprop
from keras.utils.data_utils import get_file
import tensorflow.keras
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import random
import sys
import io
import pickle
from google.colab import drive
from google.colab import files

# Монтируем связь с хранилищем Google.Drive
drive.mount('/content/gdrive')
!mkdir -p /content/gdrive/'My Drive'/saved_nnets
!mkdir -p /content/gdrive/'My Drive'/saved_nnets/war_and_peace
!mkdir -p /content/gdrive/'My Drive'/saved_nnets/corpus_ru
!mkdir -p /content/gdrive/'My Drive'/saved_nnets/silver_standard

# Загружаем данные для обучения
files.upload("silver_standard.txt")	# ГИКРЯ
files.upload("corpus.ru.txt")		# Яндекс.Перевод
files.upload("war_and_peace.txt")	# "Война и мир"

# Создаём пути к обучающим выборкам
# еред запуском выбрать одно из трёх
path = '/content/gdrive/My Drive/saved_nnets/war_and_peace.txt'
path = '/content/gdrive/My Drive/saved_nnets/corpus.ru.txt'
path = '/content/gdrive/My Drive/saved_nnets/silver_standard.txt'

# Читаем корпус в память
with io.open(path, encoding='utf-8') as f:
    text = f.read().lower()

text[:50]

# Получаем информацию о символах в тексте
chars = sorted(list(set(text)))

# Готовим словари индексов для символов
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

# Задаём параметр обучения
context_length = 40					 # Длина контекста
initial_words = 4                    # Количество опорных слов в начале предложения
sentence_length = initial_words + 1  # Минимальная длина предложенй, учавствующих в проверке
prefix = 3                           # Количество опорных символов в начале слова
ending_length = 2                    # Длина окончания
step = 10							 # Шаг, с которым текст разбивается на контексты

# Превращаем текст в обучающую выборку
sentences = []
next_chars = []

for i in range(0, len(text) - context_length, step):
    r_bound = context_length + i
    sentences.append(text[i : r_bound])
    next_chars.append(text[r_bound])

# Векторизация текста в формате One Hot Encoding
x = np.zeros((len(sentences), context_length, len(chars)), dtype=np.bool)
y = np.zeros((len(sentences), ending_length, len(chars)), dtype=np.bool)
for i, sentence in enumerate(sentences):
    for t, char in enumerate(sentence):
        x[i, t, char_indices[char]] = 1
    y[i, t, char_indices[next_chars[i]]] = 1

# Создаем нейронную сеть
def create_model(context_length, char_num):
    model = Sequential()
    model.add(LSTM(128, input_shape=(context_length, char_num)))
    model.add(Dense(char_num, activation='softmax'))
   
    return model

model = create_model(context_length, len(chars))

# Создаем callback для сохранения сети
checkpoint = ModelCheckpoint("-".join([path, "{epoch:02d}.hdf5"])

# Создаём callback для своевременной приостановки обучения
early_stop = EarlyStopping(monitor='val_loss', verbose=1, mode='min')

# Задаём свои настройки оптимизатора для повышения точности обучения
# Это сделает обучение медленнее, однако позволит обойтись меньшим числом эпох
custom_adam = keras.optimizers.Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-8, decay=0.0, amsgrad=False)

model.compile(loss='categorical_crossentropy',
              metrics = ["accuracy"],
              optimizer=custom_adam)

# Запускаем обучение нейронной сети
history = model.fit(x, y,
          batch_size=128,
          epochs=30,
          validation_split=0.2,
          initial_epoch=0,
          callbacks=[checkpoint,early_stop],
          verbose=1)

# Сохраняем объект History для последующего тестирования качества модели
!mkdir -p /content/gdrive/'My Drive'/saved_nnets/history
history_path = "/content/gdrive/My Drive/saved_nnets/history/history_silver_standard"
history_path = "/content/gdrive/My Drive/saved_nnets/history/history_corpus_ru"
history_path = "/content/gdrive/My Drive/saved_nnets/history/history_war_and_peace"

with io.open(history_path, 'wb') as h:
	pickle.dump(history.history, h)